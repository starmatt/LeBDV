<?php

return [
    'production' => true,
    'baseUrl' => '',
    'collections' => [],
    'sitename' => 'Véronique Durau',
    'contact_mail' => 'veroniquedurau@gmail.com',
    'contact_phone' => '0686202423',
    'contact_phone_p' => '06 86 20 24 23',
    'selected' => function ($page, $section) {
        return str_contains($page->getPath(), $section) ? 'selected' : '';
    },
];
