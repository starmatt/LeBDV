@extends('_layouts.master')

@section('body')
<h1 class="title mb-4">Contact</h1>

<div class="p-4 bg-std">
    <h5 class="mt-0 mb-4">Informations</h5>

    <div class="media mb-4">
        <div class="align-self-center mr-4 text-yellow"><i class="fas fa-envelope fa-fw info-icon"></i></div>

        <div class="media-body">
            <p class="mb-0"><b>Adresse e-mail :</b> <a class="text-decoration-none" href="mailto:{{ $page->contact_mail }}">{{ $page->contact_mail }}</a></p>
        </div>
    </div>

    <div class="media mb-4">
        <div class="align-self-center mr-4 text-yellow"><i class="fas fa-phone fa-fw info-icon"></i></div>

        <div class="media-body">
            <p class="mb-0"><b>Téléphone :</b> <a class="text-decoration-none" href="tel:{{ $page->contact_phone }}">{{ $page->contact_phone_p }}</a></p>
        </div>
    </div>

    <div class="media mb-4">
        <div class="align-self-center mr-4 text-yellow"><i class="fas fa-home fa-fw info-icon"></i></div>

        <div class="media-body">
            <address class="mb-0">
                <b>Véronique Durau</b><br>547 avenue Thérèse<br>94500 Champigny-sur-Marne<br>IDF France
            </address>
        </div>
    </div>

    <div class="media">
        <div class="align-self-center mr-4 text-yellow"><i class="fas fa-hashtag fa-fw info-icon"></i></div>

        <div class="media-body">
            <p class="mb-0"><b>SIRET :</b> XXX XXX XXX XXXXX<br><b>TVA :</b> FR XX XXX XXX XXX</p>
        </div>
    </div>
</div>

@endsection
