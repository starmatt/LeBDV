function toggleNav(event) {
    event.preventDefault();

    var icon = document.querySelector('#menu-icon');
    var nav = document.querySelector('#mobile-nav');
    var isOpen = nav.classList.contains('is-open');

    !isOpen ? icon.classList.replace('fa-bars', 'fa-times') : icon.classList.replace('fa-times', 'fa-bars');

    nav.classList.toggle('is-open');
}

window.addEventListener('load', function () {
    var navButton = document.querySelector('#nav-button');

    navButton.addEventListener('click', toggleNav);
});
