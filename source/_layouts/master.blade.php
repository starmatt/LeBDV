<!DOCTYPE html>
<html lang="fr">
<head>
    <title>{{ $page->sitename }} | {{ $page->getPath() }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
</head>

<body>
    <header class="desktop d-none d-md-block">
        <nav class="top-nav container-fluid">
            <ul>
                <li><a href="mailto:{{ $page->contact_mail }}"><i class="fas fa-envelope"></i> {{ $page->contact_mail }}</a></li>
                <li><a href="tel:{{ $page->contact_phone }}"><i class="fas fa-phone"></i> {{ $page->contact_phone_p }}</a></li>
            </ul>
        </nav>

        <div class="brand">
            <div class="container">
                <div class="sitename"><a href="/">{{ $page->sitename }}</a></div>
                <div class="phrase">Assistante administrative indépendante</div>
            </div>
        </div>

        <nav class="bot-nav container-fluid">
            <ul>
                <li><a class="{{ $page->getPath() === '' ? 'selected' : '' }}" href="/">Accueil</a></li>
                <li><a class="{{ $page->selected('prestations_entreprises') }}" href="/prestations_entreprises">Pour les entreprises</a></li>
                <li><a class="{{ $page->selected('prestations_particuliers') }}" href="/prestations_particuliers">Pour les particuliers</a></li>
                <li><a class="{{ $page->selected('tarifs') }}" href="/tarifs">Tarifs</a></li>
                <li><a class="{{ $page->selected('contact') }}" href="/contact">Contact</a></li>
            </ul>
        </nav>
    </header>

    <header class="mobile d-md-none">
        <div class="mobile-header container-fluid">
            <div class="mobile-brand">
                <a href="/">{{ $page->sitename }}</a>
            </div>

            <div id="nav-button">
                <a href="#"><i id="menu-icon" class="fas fa-bars fa-fw"></i></a>
            </div>
        </div>

        <nav id="mobile-nav">
            <ul>
                <a href="/"><li class="{{ $page->getPath() === '' ? 'selected' : '' }}">Accueil</li></a>
                <a href="/prestations_entreprises"><li class="{{ $page->selected('prestations_entreprises') }}" >Pour les entreprises</li></a>
                <a href="/prestations_particuliers"><li class="{{ $page->selected('prestations_particuliers') }}">Pour les particuliers</li></a>
                <a href="/tarifs"><li class="{{ $page->selected('tarifs') }}">Tarifs</li></a>
                <a href="/contact"><li class="{{ $page->selected('contact') }}">Contact</li></a>
            </ul>
        </nav>
    </header>

    <div class="main my-5">
        <div class="container">
            @yield('body')
        </div>
    </div>

    <footer>
        <div class="container">
            Réalisation et développement par <a href="mailto:starmatt@free.fr">Matthieu Borde</a>
        </div>
    </footer>

    <script type="text/javascript" src="{{ mix('js/main.js', 'assets/build') }}"></script>
</body>
</html>
