@extends('_layouts.master')

@section('body')

<h1 class="title mb-4">Prestations pour les entreprises</h1>

<div class="p-4 bg-std">
    <p class="m-0">Mes prestations de secrétariat, d'assistanat et de bureautique s'adressent <b>aux entreprises, artisans, commerçants, professions libérales</b> mais aussi <b>aux associations et aux particuliers...</b></p>

    <div class="text-center mt-4">
        <a class="btn btn-std" href="/tarifs">Voir les tarifs</a>
    </div>

    <hr class="my-4">

    <div class="media">
        <div class="media-icon right d-md-none">
            <div class="icon-wrapper"><i class="far fa-handshake"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Gestion administrative de votre entreprise</h5>

            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Recherche, préparation & suivi des pièces comptables & administratives en relation avec votre expert-comptable, ou votre C.G.A.</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Classement, tri, archivage et numérisation de tout type de document</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Saisie et suivi des factures, devis, bons de commande clients/fournisseurs</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Elaboration des modèles uniques de factures, devis, bons de commande pour les entreprises en cours de création</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création de tableau Word/Excel avec ou sans formules</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création de statistiques avec organigramme et/ou graphique</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Saisie de vos fichiers clients</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Relances téléphoniques et écrites pour impayés clients</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Mise sous pli de prospectus, courriers et autres</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Dépôt à La Poste</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Achats de vos fournitures de bureaux</li>
            </ul>
        </div>

        <div class="media-icon right d-none d-md-flex">
            <div class="icon-wrapper"><i class="far fa-handshake"></i></div>
        </div>
    </div>

    <div class="media">
        <div class="media-icon left">
            <div class="icon-wrapper"><i class="fas fa-file-alt"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Démarches auprès de l'administration</h5>

            <p>Assistance aux démarches administratives ou prise en main de vos dossiers par mes soins et entrée en relation directe avec les organismes :</p>
            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Sociaux (R.S.I., URSSAF, M.S.A.)</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Fiscaux (Centre des impôts [CDI], Service des Impôts des entreprises [SIE], Centre des  impôts fonciers [CDIF]</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Conservation des hypothèques [CH]</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Bancaires & postaux</li>
            </ul>
        </div>
    </div>

    <div class="media">
        <div class="media-icon d-md-none">
            <div class="icon-wrapper"><i class="far fa-comment"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Communication & Marketing</h5>

            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Gestion du fichier client avec envoi personnalisé</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Réalisation de maquettes pour flocage véhicule, panneaux publicitaires</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Réalisation de maquettes publicitaires personnalisées, flyers, cartes de visite (Recto-verso, quadrichromie, tous formats)</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création de votre logo d’entreprise</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Recherche pertinente de votre slogan</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Accompagnement dans votre projet de communication (sur un produit, sur l'image de votre entreprise, sur une recherche de clientèle type, sur un événement)</li>
            </ul>
        </div>

        <div class="media-icon right d-none d-md-flex">
            <div class="icon-wrapper"><i class="far fa-comment"></i></div>
        </div>
    </div>

    <div class="media">
        <div class="media-icon left">
            <div class="icon-wrapper"><i class="fas fa-desktop"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Site web & Mailing</h5>

            <ul class="fa-ul mb-0">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création et entretien de sites internet</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Référencement de sites internet</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Mise en forme et retouche des photos</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Campagne publicitaire, mailing et publipostage</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Gestion de votre e-mail</li>
            </ul>
        </div>
    </div>
</div>

@endsection
