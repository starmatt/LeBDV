@extends('_layouts.master')

@section('body')

<h1 class="title mb-4">Qui suis-je ?</h1>

<div class="p-4 bg-std mb-5">
    <div class="media">
        <img src="https://placekitten.com/150" class="media-pic left" alt="Véronique Durau">

        <div class="media-body">
            <h5 class="mt-0 mb-3">Assistante administrative, comptable, commerciale et juridique</h5>

            <p>Forte de 30 ans d’expérience en qualité d'Assistante de Direction dans un cabinet d’expertise comptable, je désire mettre mes compétences et mes connaissances à votre <b>service</b> !</p> 
            <p>De formation professionnelle en <b>Secrétariat, Assistance, Conseil</b>, j'ai toujours eu à cœur d'épauler les autres dans leurs projets et affaires. Notre collaboration se révèlera être un véritable atout et une aide précieuse à la gestion de votre activité et de votre emploi du temps.</p>
        </div>
    </div>

    <p>Mon parcours m'a permis d'affiner mon savoir-faire dans l'assistanat de direction, la gestion administrative, commerciale, comptable et juridique ainsi que la prise en main de logiciels tels que <b>le pack office, EBP, CIEL, FIDU-EXPERT...</b> et de l'outil Internet dont plus aucune entreprise ne peut se passer.</p>
    <p>Ce secteur d’activité a aussi enrichi mes relations avec les services de l'administration Française, les rouages des experts-comptables et des différents organismes tels que <b>MSA, RSI, URSSAF, IMPOTS</b>, sont pour moi  des terrains connus dont j'ai compris les mécanismes et avec lesquels j'ai pu travailler ma persévérance et ma patience.</p>

    <div class="text-center mt-4">
        <a class="btn btn-std" href="/prestations_entreprises">En savoir plus</a>
    </div>
</div>

<h1 class="title mb-4">Votre assistante à distance</h1>

<div class="p-4 bg-std">
    <div class="media">
        <div class="media-icon d-md-none">
            <div class="icon-wrapper"><i class="fas fa-wifi fa-fw"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Vous me confiez une mission et je la réalise à distance</h5>

            <p>La  flexibilité et les gains qu'apportent le télétravail ne sont plus à démontrer (gain d'espace, de charges, de temps, d'efficacité...).</p>
            <p>Les moyens de communication d'aujourd'hui permettent l'échange d'information en temps réel et votre assistante est joignable en journée continue du lundi au vendredi.</p>
            <p>Toutefois, si vous souhaitez que certaines informations restent physiquement au sein de votre entreprise ou pour des missions nécessitant mon intervention dans vos locaux, je suis mobile et me rends sur site.</p>
        </div>

        <div class="media-icon right d-none d-md-flex">
            <div class="icon-wrapper"><i class="fas fa-wifi fa-fw"></i></div>
        </div>
    </div>

    <div class="media">
        <div class="media-icon left">
            <div class="icon-wrapper"><i class="fas fa-chart-line fa-fw"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Les principaux avantages à utiliser cette méthode</h5>

            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Vous restez libre de tout engagement</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Vous ne supportez pas le coût d’un salarié, ni les contraintes de recrutement et de déclaration</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Vous maitrisez la flexibilité : notre partenariat sera ponctuel ou régulier en fonction de vos besoins</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Vous ne payez que le service rendu</li>
            </ul>
        </div>
    </div>
</div>

@endsection
