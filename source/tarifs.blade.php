@extends('_layouts.master')

@section('body')
<h1 class="title mb-4">Les tarifs</h1>

<div class="p-4 bg-std no-border">
    <p class="mb-0">Les tarifs varient selon la nature de la mission, sa durée et le lieu de l’intervention de celle-ci (le site du client ou à domicile). Vous pouvez, en consultant les différentes possibilités qui s’offrent à vous ci-après, réfléchir à votre besoin et affiner votre demande.</p>

    <hr class="my-4">

    <div class="mb-4">
        <h5><i class="fas fa-angle-right fa-fw text-yellow"></i> Vous avez besoin d'une assistante sur site et vous avez déjà défini votre besoin en heures</h5>
        <p class="mb-0">Optez pour le forfait. De quelques heures à plusieurs semaines, vous achetez un crédit d’heures et le répartissez comme bon vous semble sur la période qui vous intéresse. De plus, ces forfaits sont dégressifs (plus vous achetez d’heures, plus le taux horaire est intéressant pour vous).</p>
    </div>

    <div class="mb-4">
        <h5><i class="fas fa-angle-right fa-fw text-yellow"></i> Vous me missionnez pour une tâche bien spécifique</h5>
        <p class="mb-0">Rédaction d’une brochure, organisation d’un événement, archivage, numérisation de vos dossiers etc... En  fonction  de  votre besoin, je vous établis un devis comprenant mes honoraires pour l’intégralité de la mission, peu importe le temps passé à la réalisation de celle-ci.</p>
    </div>

    <div class="mb-4">
        <h5><i class="fas fa-angle-right fa-fw text-yellow"></i> Vous n’avez pas de budget défini, ni de temps imparti pour traiter votre dossier</h5>
        <p class="mb-0">Nous fixons alors ensemble un taux horaire et je vous facture au temps passé.</p>
    </div>

    <div class="mb-0">
        <h5><i class="fas fa-angle-right fa-fw text-yellow"></i> Vous  avez  un  budget  bien défini et ne voulez ni ne pouvez le dépasser</h5>
        <p class="mb-0">Regardons ensemble à combien d’heures votre budget correspond et si la réalisation de la mission est envisageable.</p>
    </div>

    <hr class="my-4">

    <p class="mb-0 text-center">Dans tous les cas, <b>n'hésitez pas à me contacter</b>, nous pourrons ainsi parler plus en détail de votre besoin et trouver des  solutions efficaces et adaptées à votre situation.</p>
</div>

<div class="mb-5">
    <table class="table table-responsive table-bordered rates mb-0">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">1 heure</th>
                <th scope="col">½ journée</th>
                <th scope="col">1 journée</th>
                <th scope="col">Forfait mensuel* 20h</th>
                <th scope="col">Forfait mensuel* 35h</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">Montant</th>
                <td class="text-monospace text-right">30€</td>
                <td class="text-monospace text-right">85€</td>
                <td class="text-monospace text-right">190€</td>
                <td class="text-monospace text-right">475€</td>
                <td class="text-monospace text-right">770€</td>
            </tr>
            <tr>
                <th scope="row">Tarif horaire</th>
                <td class="text-monospace text-right">30€</td>
                <td class="text-monospace text-right">28€</td>
                <td class="text-monospace text-right">27€</td>
                <td class="text-monospace text-right">24€</td>
                <td class="text-monospace text-right">23€</td>
            </tr>
        </tbody>
    </table>
    <p class="small">* La notion de mois s'entend du premier jour au dernier jour ouvrable du mois concerné.</p>
</div>

<h1 class="title mb-4">Modalités</h1>

<div class="p-4 bg-std">
    <p>Dans tous les cas, un devis vous sera remis. Si vous êtes satisfait de ma proposition, nous signons un contrat. Selon la nature et le temps consacrés à votre mission, un acompte de 30 % pourra vous être demandé à la signature de celui-ci.</p>
    <p>La facturation intervient en fin de mois ou en fin de mission si celle-ci est inférieure à 1 mois.<br>Les honoraires sont majorés de 25% les samedis et de 50% les dimanches et jours fériés.</p>
    <p>Je vous invite à lire <a href="#0" class="text-decoration-none">les conditions générales de vente.</a></p>
    <p>Pour un client qui aura recours de façon permanente, un contrat peut être consenti et accepté pour une période indéterminée ou déterminée. Il peut être résilié à tout moment sans justification.</p>

    <div class="media">
        <div class="align-self-center mr-4 text-yellow"><i class="fas fa-info-circle fa-fw info-icon"></i></div>

        <div class="media-body">
            <p class="mb-0">Une fois notre  rendez-vous fixé, je me déplace « gratuitement » à votre bureau ou à votre domicile, afin de vous rencontrer et de définir avec vous les services dont vous avez besoin.<br>Tout devis réalisé par mes soins est gratuit et valable 3 mois !</p>
        </div>
    </div>
</div>

@endsection
