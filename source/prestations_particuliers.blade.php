@extends('_layouts.master')

@section('body')

<h1 class="title mb-4">Prestations pour les particuliers</h1>

<div class="p-4 bg-std">
    <h5 class="m-0 text-center">Assistante personnelle à domicile et support à la vie privée.</h5>

    <div class="text-center mt-4">
        <a class="btn btn-std" href="/tarifs">Voir les tarifs</a>
    </div>

    <hr class="my-4">

    <div class="media">
        <div class="media-icon d-md-none">
            <div class="icon-wrapper"><i class="fas fa-folder-open"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Assistance bureautique</h5>

            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Saisie de texte avec ou sans mise en page</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Relecture, correction, et mise en page de document</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Rédaction de courrier</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Tri, classement, numérisation et archivage de documents</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création CV (photo comprise)</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création de lettre de motivation personnalisée</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Création de tableau Word/Excel avec ou sans formules</li>
            </ul>
        </div>

        <div class="media-icon right d-none d-md-flex">
            <div class="icon-wrapper"><i class="fas fa-folder-open"></i></div>
        </div>
    </div>

    <div class="media">
        <div class="media-icon left">
            <div class="icon-wrapper"><i class="far fa-keyboard"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Aide informatique et internet</h5>

            <ul class="fa-ul">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Aide à la création d'une adresse e-mail</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Aide à la prise en main de vos courriels (gmail, outlook, ...)</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Aide et assistance à la prise en main d'internet, surf et recherche d'informations</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Formation sur logiciels de bureautique (Word, Excel) pour apprendre à saisir des documents textes/tableaux/organigrammes.., à les mettre en forme mais aussi pour retoucher des photos</li>
            </ul>
        </div>
    </div>

    <div class="media">
        <div class="media-icon d-md-none">
            <div class="icon-wrapper"><i class="fas fa-balance-scale"></i></div>
        </div>

        <div class="media-body">
            <h5 class="mt-0 mb-3">Gestion de projet et de budget</h5>

            <ul class="fa-ul mb-0">
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Evénement à organiser (mariage, anniversaire, départ en retraite, voyage)</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Renégociation de contrats (assurances, téléphone, crédits, personnels)</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Reconstitution de carrière avant départ en retraite</li>
                <li><span class="fa-li" ><i class="fas fa-angle-right"></i></span>Recherche de niches d’économies annuelles à réaliser</li>
            </ul>
        </div>

        <div class="media-icon right d-none d-md-flex">
            <div class="icon-wrapper"><i class="fas fa-balance-scale"></i></div>
        </div>
    </div>
</div>

@endsection
